module.exports = {
	changePairToNumber: function(pair){
        var number=11;
        if(pair == "GBPJPY")number=11;
        if(pair == "USDCHF")number=4;
        if(pair == "GBPUSD")number=3;
        if(pair == "USDJPY")number=2;
        if(pair == "EURUSD")number=1;
        if(pair == "AUDUSD")number=6;
        if(pair == "USDCAD")number=7;
        if(pair == "EURCHF")number=5;
        if(pair == "EURGBP")number=9;
        if(pair == "EURJPY")number=10;
        if(pair == "XAUUSD")number=4001;
        return number;
    },
    changeNumberToPair: function(pair){
        var namePair=null;
        if(pair == 11)namePair="GBPJPY";
        if(pair == 4)namePair="USDCHF";
        if(pair == 3)namePair="GBPUSD";
        if(pair == 2)namePair="USDJPY";
        if(pair == 1)namePair="EURUSD";
        if(pair == 6)namePair="AUDUSD";
        if(pair == 7)namePair="USDCAD";
        if(pair == 5)namePair="EURCHF";
        if(pair == 9)namePair="EURGBP";
		if(pair == 10)namePair="EURJPY";
        if(pair == 4001)namePair="XAUUSD";
        
        return namePair;
    }
}