const TelegramBot = require('node-telegram-bot-api');
//const token = '473894862:AAHsoLiBjiHNpkpr96Alp3Dgbk78S0wUZsI';
const token = '692400082:AAEfzZ7Pm0tzU91jg55d_ohPocSk25ZBI7E';

const bot = new TelegramBot(token, {
    polling: false
});
var chatId = "276393263";
const chalk = require('chalk');
const log = console.log;
global_socket= null;

module.exports = {
    setSocket: function (socket){
        global_socket =socket;
    },
    telegram: function(message, chatId=null){
        bot.sendMessage(chatId, message);
    },
    sendNotification: function(message, isTelegram, isDebug, isAlert, type=null){
        if(isTelegram){
            this.telegram(message);
        }
        if(isDebug){
            if(type == "danger")
                log(chalk.red(message));
            if(type == "info")
                log(chalk.blue(message));
            if(type == "success")
                log(chalk.green(message));
        }
        if(isAlert){
            if(type == "danger")
                global_socket.emit('danger', {
                    message: message
                });
            if(type == "success")
                global_socket.emit('success', {
                    message: message
                });
            if(type == "info")
                global_socket.emit('info', {
                    message: message
                });
            if(type == "warning")
                global_socket.emit('warning', {
                    message: message
                });
        }
    }
};