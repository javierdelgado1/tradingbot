var notification = require('./notification.js');
const binance = require('node-binance-api')().options({
    APIKEY: '1S09b7PWLccbBEcpCvcPwHkQRWpdb6a6orYwOveZ3rejxkXuslY4RA9331vZ0Rc9',
    APISECRET: 'JGYhuib75T78IvErofyrAWFLlqGGxSeVKqqmturYpgGwSMW3ONCldju144gs0UDZ',
    useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
    test: true, // If you want to use sandbox mode where orders are simulated
    'reconnect': true
});
const FormData = require('form-data');
const fetch = require('node-fetch');

global_socket = null;
var runningBot = false;
var symbol = "";
var stopPrice = "";
var quantity = "";
var target = "";
var price = "";
var ganancia = "";
var seCumplio = false;
var base_url = "http://criptobot.test"
var orderId = null;
var orderIdToCancel = null;
var ask = "";
var isDebug = true;
var message = "";
var continuarTrading= false;
var percentaje = null;
module.exports = {
    setValue: function (data) {
        symbol = data.symbol;
        stopPrice = data.stop;
        quantity = data.quantity;
        target = data.target;
        price = data.price;
        ganancia = data.ganancia;
        ask = data.ask;
        quantity_loss = data.loss;
        runningBot = true;
        continuarTrading= data.continuarTrading;
        percentaje = data.percentaje;

        console.log(symbol);
        console.log(stopPrice);
        console.log(quantity);
        console.log(target);
        console.log(price);
        console.log(ganancia);
        console.log(ask);
        console.log(quantity_loss);
        console.log(continuarTrading);
        console.log(percentaje);


    },
    setSocket: function (socket) {
        global_socket = socket;
        notification.setSocket(socket)
    },
    ComprobarTrade1: function (times) {
        if (times < 1) {
            return;
        }
        message = "Se ha cancelado la orden: " + orderId + " symbol: " + symbol
        notification.sendNotification(message, false, false, true, 'success');

        binance.orderStatus(symbol, orderId, (error, orderStatus, symbol) => {
            //console.log(symbol+" order status:", orderStatus.status);
            if (orderStatus.status == "FILLED") {
                message = "Se ha cancelado la orden: " + orderId + " symbol: " + symbol
                notification.sendNotification(message, true, false, true, 'success');
                runningBot = true;
                seProceso = true;
                times = 0;
                createOrder("sell", quantity_loss, price, stopPrice, symbol);
                runsocket();
                log(chalk.green('Trade 1 buy limit ejecutado'));
                notification.sendNotification(message, true, false, true, 'success');
            }

            if ((times - 1) == 1) {
                cancelOrder(orderIdToCancel, symbol);
                var message = 'Se cancelo el trading porque el precio cambio muy rapido';
                notification.sendNotification(message, false, true, true, 'damger');
            }
        });

        /* if(isDebug && times == 5 ){
             runningBot = true;
             seProceso=true;
             times=0;
             websockets();
             log(chalk.green('Trade 2 buy limit ejecutado'));
         }*/


        setTimeout(function () {
            message = 'Esperando que se ejecute buy limit trade 1';
            notification.sendNotification(message, false, true, false, 'info');
            ComprobarTrade1(times - 1);
        }, 2000);
    },
    ComprobarTrade2: function(times) {
        if (times < 1) {
            return;
        }
        binance.orderStatus(symbol, orderId, (error, orderStatus, symbol) => {
            //console.log(symbol+" order status:", orderStatus.status);
            if (orderStatus.status == "FILLED") {
                runningBot = false;
                times = 0;
                message= 'Trade 2 sell limit ejecutado';
                notification.sendNotification(message, false, isDebug, false, 'success');
                message= "Se ha ejecutado la orden " + orderId + " trade 2 sell limit";
                notification.sendNotification(message, true, false, false, 'success');
            }
        });
        setTimeout(function () {
            message= 'Esperando que se ejecute sell limit  trade 2';
            notification.sendNotification(message, false, isDebug, false, 'success');
            ComprobarTrade2(times - 1);
        }, 2000);
    },
    trade: function (typeTrade, symbol, quantity, target) {
        const form = new FormData();
        form.append("symbol", symbol);
        form.append("quantity", quantity);
        form.append("price", target);
        let endpoint = "";
        if (typeTrade == "sell") {
            endpoint = "/binance/limitsell";
        }
        if (typeTrade == "buy") {
            endpoint = "/binance/limitbuy";
        }

        fetch(base_url + endpoint, {
                method: 'POST',
                headers: form.getHeaders(),
                body: form
            })
            .then(res => res.json()) // expecting a json response
            .then(json => {
                orderId = json['orderId'];
                message = "Se ha creado una orden limit " + typeTrade + "\n Symbol: " + symbol + "\nPrice: " + price + "\nQuantity: " + quantity + "\n orderId: " + orderId;
                notification.sendNotification(message, true, false, true, 'info');

                if (json['code'] != -1013) {
                    ComprobarTrade1(120);
                    if (isDebug) {
                        log(chalk.blue("Orden Creada  " + typeTrade + " limit " + orderId));
                    }
                } else {
                    message = "Error con el minimo de inversion";
                    notification.sendNotification(message, false, true, true, 'danger');
                }

                if (typeTrade == "sell") {
                    ComprobarTrade2(120);
                }
            });
    },
    cancelOrder: function (symbol, orderId, executeOrder = false) {
        let res = false;
        binance.cancel(symbol, orderId, (error, response, symbol) => {
            res = response;
            message = "Se ha cancelado la orden: " + orderId + " symbol: " + symbol
            notification.sendNotification(message, true, true, true, 'info');
            if (executeOrder)
                trade("sell", symbol, quantity, target);
        });
        return res;
    },
    openOrders: function () {
        binance.openOrders(false, (error, openOrders) => {
            console.log("openOrders()", openOrders);
            global_socket.emit('openOrders', {
                orders: openOrders
            });
        });
    },
    runsocket: function () {
        
            binance.websockets.candlesticks([symbol], "1m", (candlesticks) => {
                let {
                    e: eventType,
                    E: eventTime,
                    s: symbol,
                    k: ticks
                } = candlesticks;
                let {
                    o: open,
                    h: high,
                    l: low,
                    c: close,
                    v: volume,
                    n: trades,
                    i: interval,
                    x: isFinal,
                    q: quoteVolume,
                    V: buyVolume,
                    Q: quoteBuyVolume
                } = ticks;
                /*console.log(symbol+" "+interval+" candlestick update");
                console.log("open: "+open);
                console.log("high: "+high);
                console.log("low: "+low);
                console.log("close: "+close);
                console.log("volume: "+volume);
                console.log("isFinal: "+isFinal);*/
                if (isDebug)
                    log(chalk.red("Fecha: " + Date.now()));
                if (runningBot)
                    trading(open);
            });
    },
    createOrder: function (typeMarket, quantity_loss, price, stopPrice, symbol) {
        //console.log(symbol)
        //console.log(stopPrice)
        ///console.log(quantity)
        // console.log(target)
        //console.log(price)
        //console.log(typeMarket)
        /*console.log(JSON.stringify({
            "symbol": symbol,
            "side": typeMarket,
            "quantity": quantity,
            "stopPrice": stopPrice,
            "price": price
        }))*/
    
        const form = new FormData();
        form.append("symbol", symbol);
        form.append("side", typeMarket);
        form.append("quantity", quantity_loss);
        form.append("stopPrice", stopPrice);
        form.append("price", price);
        fetch(base_url + "/binance/createOrder", {
                method: 'POST',
                headers: form.getHeaders(),
                body: form
            })
            .then(res => res.json()) // expecting a json response
            .then(json => {
                if (json['code'] == -2010) {
                    message = "Ocurrio un problema al crear el STOP LOSS, se cancelo ";
                    notification.sendNotification(message, false, true, true, 'danger');
                } else {
                    orderIdToCancel = json['orderId'];
                    message = "Se ha creado una orden STOP LOSS \nSymbol: " +
                        symbol + "\nSide: " + typeMarket + "\nQuantity: " + quantity + "\nstopPrice: " + stopPrice + "\nPrice: " + price;
                    notification.sendNotification(message, true, false, true, 'danger');
                    if (isDebug) {
                        log(chalk.blue("STOP LOSS Creada  " + orderIdToCancel));
                    }
                }
            });
    },
    ComprobarOrder:function(order, symbol) {
        binance.orderStatus(symbol, order, (error, orderStatus, symbol) => {
            //console.log(symbol+" order status:", orderStatus.status);
            if (orderStatus.status == "FILLED") {
                runningBot = false;
            }
            //console.log(status)
    
        });
    },
    trading:function(first) {
        if (isDebug)
            log(chalk.blue("¿Se cumplio la meta? " + seCumplio + " price: " + first + " target: " + target));
        if (first >= target && !seCumplio) {
            if(!continuarTrading){
                seCumplio = true;
                runningBot = false;
                message= "Se cumplio el target " + target + " se cancelara el STOP LOSS y se creara nueva orden LIMIT SELL";
                notification.sendNotification(message, true, false, true, 'success');
                if (isDebug) {
                    log(chalk.green("se cumplo la meta"));
                    log(chalk.red("Cancelando STOP LOSS"));
                    log(chalk.blue("Creando LIMIT SELL Trade 2"));
                }
                cancelOrder(symbol, orderIdToCancel);
            }
            else{
                price= target;
                stopPrice=target;
                target+=percentaje;
                message= "Se cumplio el target " + target + " se colocara un STOP LOSS en el nuevo target";
                notification.sendNotification(message, true, false, true, 'success');
                cancelOrder(symbol, orderIdToCancel);
                createOrder("sell", quantity_loss, price, stopPrice, symbol);
            }
            
            //vendo lo que compre cuando inice el trading  a precio target
        }
        if (first <= price) {
            message = "Se ejecuto el  STOP LOSS";
            notification.sendNotification(message, true, isDebug, false, 'warning');
        }
    }
};