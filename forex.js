"use strict";
//
// begin Load and check connection parameters
//
const indicators = require('./indicators.js');
const moment = require('moment-timezone');
var tools = require('./rsi.js')
const chalk = require('chalk');
const log = console.log;
var request = require('request');
var notification = require('./notification.js');
//const EMA = require('technicalindicators').EMA;
var global = require('./global.js')
var today = moment().tz("America/Caracas").format("M/D/YYYY").toString();
var config;
try {
	config = require('./config.js');
} catch (e) {
	console.log('Error loading config.js. Please rename or copy config.sample.js into config.js');
	process.exit();
}
var token = config.token;
var trading_api_host = config.trading_api_host;
var trading_api_port = config.trading_api_port;
var trading_api_proto = config.trading_api_proto;
if (typeof (token) === 'undefined' || typeof (trading_api_host) === 'undefied' || typeof (trading_api_port) === 'undefined' || typeof (trading_api_proto) === 'undefined') {
	console.log('config.js contents error');
	process.exit();
}
if (token === 'PASTE_YOUR_TOKEN_HERE') {
	console.log('please paste your token in config.js file');
	process.exit();
}
//
// end Load and check connection parameters
//

//
// begin Internal variables and objects
//
const result = null;
var EventEmitter = require('events');
var cli = new EventEmitter();
var io = require('socket.io-client');
var socket;
var querystring = require('querystring');
var tradinghttp = require(trading_api_proto);
var globalRequestID = 1;
var request_headers = {
	'User-Agent': 'request',
	'Accept': 'application/json',
	'Content-Type': 'application/x-www-form-urlencoded'
}
//
// end Internal variables and objects
//

//
// begin Core functionality
//
var getNextRequestID = () => {
	return globalRequestID++;
}

var default_callback = (statusCode, requestID, data) => {
	if (statusCode === 200) {
		try {
			var jsonData = JSON.parse(data);
		} catch (e) {
			console.log('request #', requestID, ' JSON parse error:', e);
			return;
		}
		console.log('request #', requestID, ' has been executed:', JSON.stringify(jsonData, null, 2));
		var datos = [];
		for (var i = 0; i < data.length; i++) {
			datos.push({
				open: parseFloat(data[i][1]),
				close: parseFloat(data[i][2]),
				high: parseFloat(data[i][3]),
				low: parseFloat(data[i][8]),
				//date: moment(data[i][0]).tz("America/Caracas").format("D/M/YYYY hh:mm").toString()
			});
		}
		console.log("datos: ")
		console.log(datos)
		return data;
	} else {
		console.log('request #', requestID, ' execution error:', statusCode, ' : ', data);
	}
}

var request_processor = (method, resource, params, callback) => {
	var requestID = getNextRequestID();
	if (typeof (callback) === 'undefined') {
		callback = default_callback;
		console.log('request #', requestID, ' sending');
	}
	if (typeof (method) === 'undefined') {
		method = "GET";
	}

	// GET HTTP(S) requests have parameters encoded in URL
	if (method === "GET") {
		resource += '/?' + params;
	}

	var req = tradinghttp.request({
		host: trading_api_host,
		port: trading_api_port,
		path: resource,
		method: method,
		headers: request_headers
	}, (response) => {
		var data = '';
		response.on('data', (chunk) => data += chunk); // re-assemble fragmented response data
		response.on('end', () => {
			callback(response.statusCode, requestID, data);
		});
	}).on('error', (err) => {
		callback(0, requestID, err); // this is called when network request fails
	});

	// non-GET HTTP(S) reuqests pass arguments as data
	if (method !== "GET" && typeof (params) !== 'undefined') {
		req.write(params);
	}
	req.end();
};

// FXCM REST API requires socket.io connection to be open for requests to be processed
// id of this connection is part of the Bearer authorization
var authenticate = (token) => {
	socket = io(trading_api_proto + '://' + trading_api_host + ':' + trading_api_port, {
		query: querystring.stringify({
			access_token: token
		})
	});
	// fired when socket.io connects with no errors
	socket.on('connect', () => {
		console.log('Socket.IO session has been opened: ', socket.id);
		request_headers.Authorization = 'Bearer ' + socket.id + token;
		
	});
	// fired when socket.io cannot connect (network errors)
	socket.on('connect_error', (error) => {
		console.log('Socket.IO session connect error: ', error);
	});
	// fired when socket.io cannot connect (login errors)
	socket.on('error', (error) => {
		console.log('Socket.IO session error: ', error);
	});
	// fired when socket.io disconnects from the server
	socket.on('disconnect', () => {
		console.log('Socket disconnected, terminating client.');
		process.exit(-1);
	});
}
//
// end Core functionality
//

//
// begin Setup CLI
//

// this is called on console input
process.stdin.on('data', function (data) {
	var input = data.toString().trim();

	// if the line was empty we don't want to do anything
	if (input === '') {
		cli.emit('prompt');
		return;
	}

	// split input into command and parameters
	var inputloc = input.search('{');
	if (inputloc === -1) {
		inputloc = input.length;
	}
	var command = input.substr(0, inputloc).trim();
	var params = input.substr(inputloc).trim();

	// command must be registered with cli
	if (cli.eventNames().indexOf(command) >= 0) {
		if (params.length > 0) {
			try {
				cli.emit(command, JSON.parse(params));
			} catch (e) {
				console.log('could not parse JSON parameters: ', e);
			}
		} else {
			cli.emit(command, {});
		}
		cli.emit('prompt');
	} else {
		console.log('command not found. available commands: ', cli.eventNames());
	}
});

cli.on('prompt', () => {
	process.stdout.write('> ');
});

cli.on('exit', () => {
	process.exit();
});

// loading of extra modules
cli.on('load', (params) => {
	if (typeof (params.filename) === 'undefined') {
		console.log('command error: "filename" parameter is missing.')
	} else {
		var test = require(`./${params.filename}`);
		test.init(cli, socket);
	}
});

// helper function to send parameters in stringified form, which is required by FXCM REST API
cli.on('send', (params) => {
	if (typeof (params.params) !== 'undefined') {
		params.params = querystring.stringify(params.params);
	}
	cli.emit('send_raw', params);
});

// will send a request to the server
cli.on('send_raw', (params) => {
	// avoid undefined errors if params are not defined
	if (typeof (params.params) === 'undefined') {
		params.params = '';
	}
	// method and resource must be set for request to be sent
	if (typeof (params.method) === 'undefined') {
		console.log('command error: "method" parameter is missing.');
	} else if (typeof (params.resource) === 'undefined') {
		console.log('command error: "resource" parameter is missing.');
	} else {
		console.log("entro aqui 1")
		request_processor(params.method, params.resource, params.params, params.callback);
		console.log("entro aqui 2")

	}
});
//
// end Setup CLI
//

//
// begin Main
//
authenticate(token);
cli.emit('prompt');
//
// end Main
//

//getCandles()
module.exports = {
	getCandles: function (par, window, number, patron, emaFast = null, emaslow = null) {
		var respuesta = null;
		var req = tradinghttp.request({
			host: trading_api_host,
			port: trading_api_port,
			path: "/candles/" + par + "/" + window + "/?num=" + number,
			method: "GET",
			headers: request_headers
		}, (response) => {			
				var data = '';
				var datos = [];
				response.on('data', (chunk) => data += chunk); // re-assemble fragmented response data
				response.on('end', () => {
					var result = this.callback(response.statusCode, data);
					var originalData=result;
					if (patron == "rsi") {
						this.validateOverFlow(result,originalData, par, window);
					}
					if (patron == "rsiMod") {
						this.validateOverFlowMod(result,originalData, par, window);
					}
					if( patron == "spike"){
						this.validateSpike(result, originalData, par, window);
					}
					if( patron == "ema"){
						this.validateEma(result, originalData, par, window, emaFast, emaslow);
					}
				});
		}).on('error', (err) => {

		});

		req.end()

	},
	getEnd(req) {
		return req.end();
	},
	validateOverFlow(result,data,pair, timeframe) {
		result = indicators.parseRs(result);
		tools.rsi(result, (result.length - 1), 14)
		var over = false;
		var under = false;
		var time=null;
		for (var i = 0; i < result.length; i++) {
			var dateVela =  moment.unix(data[i].unixDate).tz("America/Caracas").format("M/D/YYYY").toString();
			if (String(dateVela) === String(today)) {
				if (result[i].rsi >= 70 && !over) {
					log(chalk.red("Hay sobre compra"));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					over = true;
					time=data[i].unixDate;
					this.save(pair,  result[i].date, timeframe, under, over, 0, result[i].rsi);
				}
				if (result[i].rsi <= 30 && !under) {
					log(chalk.blue("Hay sobre venta"));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					under = true;
					time=data[i].unixDate;
					this.save(pair,  result[i].date, timeframe, under, over, 0, result[i].rsi);
				}

				if (result[i].rsi <= 70 && over) {
					log(chalk.green("Normalizando sobre compra"));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					this.save(pair,  result[i].date, timeframe, under, over, 1, result[i].rsi);
					over = false;
					time=null;
				}

				if (result[i].rsi >= 30 && under) {
					log(chalk.green("Normalizando sobre  venta"));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					this.save(pair,  result[i].date, timeframe, under, over, 1, result[i].rsi);
					under = false;
					time=null;
				}

				/*if (result[i].rsi >= 50 && result[i].rsi <= 70 && !over) {
					//log(chalk.red(""));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					over = true;
					time=data[i].unixDate;
					this.save(pair,  result[i].date, timeframe, under, over, 0, result[i].rsi, true);
				}
				if (result[i].rsi <= 50 &&  result[i].rsi >=30 && !under) {
					log(chalk.blue("Hay sobre venta"));
					console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					under = true;
					time=data[i].unixDate;
					this.save(pair,  result[i].date, timeframe, under, over, 0, result[i].rsi, true);
				}*/
			}
		}
	},
	validateOverFlowMod(result,data,pair, timeframe) {
		result = indicators.parseRs(result);
		tools.rsi(result, (result.length - 1), 14)
		var over = false;
		var under = false;
		var time=null;
		//for (var i = 0; i < result.length; i++) {
			var dateVela =  moment.unix(data[data.length-1].unixDate).tz("America/Caracas").format("M/D/YYYY").toString();
			//if (String(dateVela) === String(today)) {
				if (result[result.length-1].rsi >= 50  ) {
					//log(chalk.red(""));
					//console.log("i: " + i + " date: " + result[result.length-1].date + " rsi: " + result[result.length-1].rsi)
					over = true;
					time=data[data.length-1].unixDate;
					this.saveRsiMod(pair,  result[result.length-1].date, timeframe, under, over, 0, result[result.length-1].rsi);
				}
				if (result[result.length-1].rsi <= 50  ) {
					//log(chalk.blue("Hay sobre venta"));
					//console.log("i: " + i + " date: " + result[i].date + " rsi: " + result[i].rsi)
					over = false;
					time=data[data.length-1].unixDate;
					this.saveRsiMod(pair,  result[result.length-1].date, timeframe, under, over, 1, result[result.length-1].rsi);
				}
			//}
		//}
	},
	validateSpike(result, data, pair, timeframe){
		var time=null;
		console.log("entro 3")
		console.log(result.length)
		console.log(today)
		for (var i = 4; i < result.length; i++) {
			var dateVela =  moment.unix(data[i].unixDate).tz("America/Caracas").format("M/D/YYYY").toString();
			console.log(result[i].date, result[i].open, dateVela)
			if (String(dateVela) === String(today)) {
				if( result[i-4].open > result[i-3].open
					&& result[i-3].open > result[i-2].open 
						&&  (result[i-2].open >= result[i-1].open )
							&& result[i-1].open <= result[i].open ){	
							console.log("i: " + i + " date: " + result[i].date + " compro a la alta")
							this.saveSpike(pair, result[i].date, timeframe, 1);
				}
				if(result[i-4].open < result[i-3].open
					&& result[i-3].open < result[i-2].open 
						&&  (result[i-2].open <= result[i-1].open  )
							&& result[i-1].open >= result[i].open ){
							console.log("i: " + i + " date: " + result[i].date + " Compro a la baja")
							this.saveSpike(pair, result[i].date, timeframe, 0);
				}
			}
		}
	},
	validateEma(result, data, pair, timeframe, emafast, emaslow){
		let period = 9;
		let values = [
			1.11522, 1.11601,1.11659,1.11870,1.11881, 1.11957, 1.11926,
			1.11937, 
			1.11995,
		];  
		//console.log("ema 1");  
		//console.log(EMA.calculate({period : period, values : values}));  
	    //console.log("ema 2");  
	    //console.log(this.emaCal(values, period));
		var dataClose = [];
		var dataClose2 = [];
		var resultAux = result;
		for (var i = 0; i < resultAux.length; i++) {
			dataClose.push({close: resultAux[i].close, date:resultAux[i].date });
		}

		for (var i = resultAux.length-emafast; i < resultAux.length; i++) {
			dataClose2.push(resultAux[i].close);
		}
		//var EmaValues1= EMA.calculate({period : emafast, values : dataClose2});
		var dataClose2 = [];
		for (var i = resultAux.length-emaslow; i < resultAux.length; i++) {
			dataClose2.push(resultAux[i].close);
		}
		//var EmaValues2= EMA.calculate({period : emaslow, values : dataClose2});
		result = indicators.parseRs(result);
		tools.rsi(result, (result.length - 1), 14);
		//console.log("Pair: "+pair+" Ema 1: ");
		//console.log(EmaValues1.length, EmaValues1[EmaValues1.length-1]);  
		//console.log("Pair: "+pair+"  Ema 2: ");
		//console.log(EmaValues2.length, EmaValues2[EmaValues2.length-1]);  
		/*console.log("Values ema 1"  );
		for (var i = 0; i < EmaValues1.length; i++) {
			console.log(EmaValues1[i]);  
		}
		console.log("Values ema 2"  );
		for (var i = 0; i < EmaValues2.length; i++) {
			console.log(EmaValues2[i]);  
		}*/
		if(EmaValues1[EmaValues1.length-1] >  EmaValues2[EmaValues2.length-1]){			
			this.saveEma(
							pair, 
							today, 
							timeframe, 
							EmaValues1[EmaValues1.length-1], 
							EmaValues2[EmaValues2.length-1], 
							result[result.length-1].rsi 
						);				
		}
		if(EmaValues1[EmaValues1.length-1] <  EmaValues2[EmaValues2.length-1]){
			this.saveEma(
							pair, 
							today, 
							timeframe, 
							EmaValues1[EmaValues1.length-1], 
							EmaValues2[EmaValues2.length-1], 
							result[result.length-1].rsi 
						);
		}
		
	},
	callback: function (statusCode, data) {
		if (statusCode === 200) {
			try {
				var jsonData = JSON.parse(data);
			} catch (e) {
				console.log(' JSON parse error:', e);
				return;
			}
			var datos = [];
			for (var i = 0; i < jsonData.candles.length; i++) {
				datos.push({
					unixDate: jsonData.candles[i][0],
					open: parseFloat(jsonData.candles[i][1]),
					close: parseFloat(jsonData.candles[i][2]),
					high: parseFloat(jsonData.candles[i][3]),
					low: parseFloat(jsonData.candles[i][8]),
					value: parseFloat(jsonData.candles[i][2]),
					date: moment.unix(jsonData.candles[i][0]).tz("America/Caracas").format("M/D/YYYY HH:mm").toString()
				});
			}
			return datos;
		} else {
			console.log(' execution error:', statusCode, ' : ', data);
		}
	},
	save: function(pair, date, timeframe, under, over, isfinally, rsi) {
		/*this.$http.post("https://criptobot.botcoin.com.ve/patron", {'pair': pair, 'date' : date  }).then(response => {
			console.log(response.body)
		}, response => {});*/
		var namePair=global.changeNumberToPair(pair);

		request.post(
			'https://criptobot.botcoin.com.ve/rsi', {
				json: {
					'pair': namePair,
					'date': date,
					'timeframe': timeframe,
					'under' : under,
					'over' : over,
					'isfinally' : isfinally,
					'rsi': rsi
				}
			},
			function (error, response, body) {
				if (!error && response.statusCode == 200) {
					if (body == "exito") {
						var fecha =  date
						var message = ""; 
						if(isfinally == 0 ){
							if(under){
								message="El par: "+namePair+ " tiene una sobreventa \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeframe: "+timeframe
								
							}
							if(over){
								message="El par: "+namePair+ " tiene una sobrecompra \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe
								
							}
							/*if(under && rsi >= 50 && isPrivate){
								message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeframe: "+timeframe
								
							}
							if(over && rsi <= 50 && isPrivate){
								message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe

							}*/
						}
						if(isfinally == 1){
							if(under){
								message="El par: "+namePair+ " esta normalizando su valor rsi \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeframe: "+timeframe
								
							}
							if(over){
								message="El par: "+namePair+ " esta normalizando su valor rsi \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe
								
							}

							/*if(under && rsi >= 50 && isPrivate){
								message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeframe: "+timeframe
								
							}
							if(over && rsi <= 50 && isPrivate){
								message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe

							}*/
						}
						//console.log(message)
						
						var joseDuarte = 780654639;
						var francisco = 335238941;
						var rafael = 464461297;
						var javier = 276393263;

						
						notification.telegram(message, joseDuarte);
						notification.telegram(message, francisco);
						notification.telegram(message, rafael);
						//notification.telegram(message, javier);
						

					}
				}

			}
		);
	},
	saveRsiMod: function(pair, date, timeframe, under, over, isfinally, rsi) {
		/*this.$http.post("https://criptobot.botcoin.com.ve/patron", {'pair': pair, 'date' : date  }).then(response => {
			console.log(response.body)
		}, response => {});*/
		var namePair=global.changeNumberToPair(pair);

		request.post(
			'https://criptobot.botcoin.com.ve/rsiMod', {
				json: {
					'pair': namePair,
					'date': date,
					'timeframe': timeframe,
					'under' : under,
					'over' : over,
					'isfinally' : isfinally,
					'rsi': rsi
				}
			},
			function (error, response, body) {
				if (!error && response.statusCode == 200) {
					console.log(error)
					console.log(body)

					if (body == "exito") {
						var fecha =  date
						var message = ""; 
						if(isfinally == 0 ){							
							message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe;
						}
						if(isfinally == 1){
							message="Posible LongShot: "+namePair+ " \nFecha: "+fecha + "\nRsi: "+rsi+"\nTimeFrame: "+timeframe;
						}
						var javier = 276393263;
						notification.telegram(message, javier);
						

					}
				}

			}
		);
	},
	saveEma: function(pair, date, timeframe,  emafast, emaslow, rsi) {
		var namePair=global.changeNumberToPair(pair);
		console.log("EmaF recibida: "+emafast, emaslow);
		var dateNow = moment().tz("America/Caracas").format("M/D/YYYY hh:mm").toString();
		request.post(
			'https://criptobot.botcoin.com.ve/ema', {
				json: {
					'pair': namePair,
					'date': date,
					'timeframe': timeframe,
					'emaFast' : emafast,
					'emaslow' : emaslow,
					'rsi' : rsi
				}
			},
			function (error, response, body) {
				console.log("response: ")
				console.log(body)
				if (!error && response.statusCode == 200) {					
					if (body == "exito") {
						var fecha =  date
						var message = "";
						console.log(emafast, emaslow)
						if(parseFloat(emafast) >= parseFloat(emaslow)){
							console.log("entro 1");
							message = "El par: "+namePair+ 
							"\nEma Fast: "+emafast + 
							"\nEma SLow: "+emaslow+
							"\nFecha: "+dateNow+
							"\nRsi: "+rsi+
							"\nTimeFrame: "+timeframe+
							"\nEma FAST sobrepasa a la ema SLOW";
						}
						if( parseFloat(emafast) <= parseFloat(emaslow)){
							console.log("entro 2");
							message = "El par: "+namePair+ 
							"\nEma Fast: "+emafast + 
							"\nEma SLow: "+emaslow+
							"\nFecha: "+fecha+
							"\nRsi: "+rsi+
							"\nTimeFrame: "+timeframe+
							"\nEma SLOW sobrepasa a la ema FAST";
						}
						console.log("message: "+message);
							
						var javier = 276393263;
						if(message != "")
							notification.telegram(message, javier);
					}
				}
			}
		);
	},
	saveSpike: function(pair, date, timeframe, direction) {
		var namePair=global.changeNumberToPair(pair);
		console.log("entro 4")

		var typeSpike = direction== 1 ? ' alta' : ' baja';
 
		request.post(
			'https://criptobot.botcoin.com.ve/spike', {
				json: {
					'pair': namePair,
					'date': date,
					'timeframe': timeframe
				}
			},
			function (error, response, body) {
				if (!error && response.statusCode == 200) {
					console.log("response: ")
					console.log(body)
					if (body == "exito") {
						var fecha =  date
						var message="El par: "+namePair+ " tiene un posible spike \nFecha: "+fecha + "\nTimeframe: "+timeframe+"\nTipo de Spike: "+typeSpike;
						var javier = 276393263;
						notification.telegram(message, javier);
					}
				}

			}
		);
	},
	emaCal: function(mArray, mRange){
		var k = 2 / (mRange + 1);
		var emaArray = [{value:mArray[0].close, date: mArray[0].date} ];
		for (var i = 1; i < mArray.length; i++) {
			var value = mArray[i].close * k + emaArray[i - 1].value * (1 - k);
			emaArray.push({ value: value, date:mArray[i].date });
		}
		return emaArray;
		//return  (a,r) => a.reduce((p,n,i) => i ? p.concat(2*n/(r+1) + p[p.length-1]*(r-1)/(r+1)) : p, [a[0]])
	}
};