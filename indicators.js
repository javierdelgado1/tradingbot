
module.exports = {
    parseRs : function(datos){
        var dataObject = [];
        var avgG = [],
            avgL = [];
        for (var i = 0; i < datos.length; i++) {
            var gain = 0.00;
            var loss = 0.00;
            var avgGain = 0.00;
            var avgLoss = 0.00;
            var rs = 0.00;
            var rsi = 0.00;
            if (i == 0) {
                avgG.push(avgGain)
                avgL.push(avgLoss)
                dataObject.push({
                    date: datos[i].date,
                    data: datos[i].value,
                    gain: gain,
                    loss: loss,
                    avgGain: 0,
                    avgLoss: 0,
                    rs: rs,
                    rsi: rsi
                });
            } else {
                var result = datos[i].value - datos[i - 1].value;
                result > 0 ? gain = result : loss = result * -1;
                avgG.push(gain)
                avgL.push(loss)
                var sumG = 0,
                    sumL = 0;
                for (var g = 0; g < avgG.length; g++) {
                    sumG += avgG[g];
                }
                avgGain = sumG / avgG.length;
    
    
                var sumG, sumL;
                for (var l = 0; l < avgL.length; l++) {
                    sumL += avgL[l];
                }
                avgLoss = sumL / avgL.length;
    
                var rs = avgLoss != 0 ? avgGain / avgLoss : 0;
                if ((i + 1) == datos.length)
                    dataObject.push({
                        date: datos[i].date,
                        data: datos[i].value,
                        gain: gain,
                        loss: loss,
                        avgGain: 0,
                        avgLoss: 0,
                        rs: 0,
                        rsi: null
                    });
                else
                    dataObject.push({
                        date: datos[i].date,
                        data: datos[i].value,
                        gain: gain,
                        loss: loss,
                        avgGain: 0,
                        avgLoss: 0,
                        rs: 0,
                        rsi: 0
                    });
            }
    
        }
        return dataObject;
    }
}